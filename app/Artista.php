<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artista extends Model
{
    protected $table = "artists";
    protected $primaryKey = "ArtistId";
    public $timestamps = false;

    //Relación artista-album
    //Metodo albunes: me va a devolver los albunes del artistas
    public function albumes(){
        return $this->hasMany('App\Album', 'ArtistId');
    }


}
