<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleados extends Model
{
    protected $table="employees";
    protected $primaryKey = "EmployeeId";
    public $timestamps = false;

    //Establecer algunos que en adelante
    //Laravel - eloquent : trata con objetos carbon
    protected $dates=['BirthDate', 'HireDate'];

    public function jefe_directo(){
        //relación muchos a uno con jfe
        return $this->belongsTo('App\Empleados', 'ReportsTo');
    }
    
    public function clientes(){
        //relacion 1 a muchos con los clientes
        return $this->hasMany("App\Cliente", 'SupportRepId');
    }


}
